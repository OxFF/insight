#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <stb_image.h>

class Texture 
{
public:
	Texture(const char * path, bool antialias)
	{
		glGenTextures(1, &this->texture);
		unsigned char* img = stbi_load(path, &this->width, &this->height, nullptr, STBI_rgb_alpha);

		if (!img)
		{
			fprintf(stderr, "Couldn't load texture '%s'", path); getchar();
			exit(-1);
		}

		glBindTexture(GL_TEXTURE_2D, this->texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->width, this->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, antialias ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, antialias ? GL_LINEAR : GL_NEAREST);

		glBindTexture(GL_TEXTURE_2D, NULL);
		stbi_image_free(img);
	}

	void bind(unsigned int sampler)
	{
		glActiveTexture(sampler);
		glBindTexture(GL_TEXTURE_2D, this->texture);
	}

	static void setWindowIcon(const char * path, GLFWwindow* window)
	{
		glfwSetWindowIcon(window, 1, createImageBuffer(path));
	}

	static void setWindowCursor(const char * path, GLFWwindow* window)
	{
		GLFWcursor* cursor = glfwCreateCursor(createImageBuffer(path), 0, 0);
		glfwSetCursor(window, cursor);

	}

	unsigned int getTexture()
	{
		return this->texture;
	}

	int getWidth()
	{
		return this->width;
	}

	int getHeight()
	{
		return this->height;
	}

	


private:
	static GLFWimage * createImageBuffer(const char* path)
	{

		GLFWimage* icons = new GLFWimage[1];
		icons[0].pixels = stbi_load(path, &icons[0].width, &icons[0].height, 0, STBI_rgb_alpha);

		return icons;
	}
	unsigned int texture;
	int width, height;
};
