#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "Input.h"
#include "Texture.h"

void framebuffer_resize_callback(GLFWwindow* window, int fbW, int fbH)
{

	glViewport(0, 0, fbW, fbH);

}

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

class Window
{
public:


	Window(int width, int height, bool fullscreen)
	{

		this->width = width;
		this->height = height;
		this->fullscreen = fullscreen;

	}

	void Init(const char* title)
	{

		if (!glfwInit())
		{
			fprintf(stderr, "Couldn't create GLFW context"); getchar();
			exit(EXIT_FAILURE);
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		glfwWindowHint(GLFW_SAMPLES, 8);

		this->window = glfwCreateWindow(width, height, title, fullscreen ? glfwGetPrimaryMonitor() : nullptr, nullptr);
		if (!this->window)
		{
			fprintf(stderr, "Couldn't create a window context"); getchar();
			glfwTerminate();
			exit(EXIT_FAILURE);
		}

		this->vidmode = (GLFWvidmode*)glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(this->window, (vidmode->width - width) / 2, (vidmode->height - height) / 2);

		this->input = new Input(window);

		glfwSetWindowAspectRatio(this->window, this->width, this->height);
		glfwSetWindowSizeLimits(this->window, 854, 480, vidmode->width, vidmode->height);

		glfwSetFramebufferSizeCallback(this->window, framebuffer_resize_callback);
		glfwSetErrorCallback(error_callback);
		glfwGetFramebufferSize(this->window, &bufferWidth, &bufferHeight);

		glfwMakeContextCurrent(this->window);

		this->deltaTime = 0.f;
		this->lastFrame = 0.f;
		this->lastTime = glfwGetTime();

		if (!gladLoadGL())
		{
			fprintf(stderr, "Couldn't create a OpenGL context"); getchar();
			glfwTerminate();
			exit(EXIT_FAILURE);
		}

		glEnable(GL_DEPTH_TEST);



		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);

		glEnable(GL_MULTISAMPLE);

		fprintf(stdout, "OpenGL %s\n", glGetString(GL_VERSION));
		fprintf(stdout, "GLFW %s\n", glfwGetVersionString());;

		
	}

	bool IsRunning()
	{
		glfwPollEvents();
		return !glfwWindowShouldClose(this->window);
	}

	void GetFramesPerSecond(double* saver)
	{
		double currentTime = glfwGetTime();
		lastFrame = currentTime;

		nbFrames++;
		if (currentTime - lastTime >= 1.0) { // If last prinf() was more than 1 sec ago
			// printf and reset timer


			*saver = nbFrames;
			nbFrames = 0;
			lastTime += 1.0;
		}
	}

	double GetDeltaTime()
	{
		double currentTime = glfwGetTime();
		deltaTime = currentTime - lastFrame;
		lastFrame = currentTime;

		return deltaTime;
	}

	void PollEvents()
	{
		glfwSwapBuffers(this->window);
		glFlush();
	}

	GLFWwindow * GetWindow()
	{
		return this->window;
	}

	Input* GetInput()
	{
		return this->input;
	}

	GLFWvidmode * GetVidMode()
	{
		return this->vidmode;
	}

	int GetWidth()
	{
		return this->width;
	}

	int GetHeight()
	{
		return this->height;
	}


private:
	GLFWwindow* window; GLFWvidmode* vidmode;
	Input* input;

	int width, height, bufferWidth, bufferHeight;
	bool fullscreen;

	double deltaTime, lastFrame, nbFrames, lastTime;
	
	

	

};



