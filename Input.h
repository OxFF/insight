#pragma once

#include <GLFW/glfw3.h>
#include <glm.hpp>

class Input 
{
public:
	Input(GLFWwindow * window)
	{
		this->window = window;
		this->keys = new bool[GLFW_KEY_LAST];
		this->mouseButtons = new bool[GLFW_MOUSE_BUTTON_LAST];

		for (int i = 0; i < GLFW_KEY_LAST; i++)
			keys[i] = false;

		for (int i = 0; i < GLFW_MOUSE_BUTTON_LAST; i++)
			mouseButtons[i] = false;
	}

	bool IsKeyDown(int key)
	{
		return glfwGetKey(window, key) == 1;
	}

	void Update()
	{
		for (int i = 32; i < GLFW_KEY_LAST; i++) {

			keys[i] = IsKeyDown(i);

		}
		
	}


	bool IsKeyPressed(int key)
	{
		return (IsKeyDown(key) && !keys[key]);
	}

	bool IsKeyReleased(int key)
	{
		return (!IsKeyDown(key) && keys[key]);
	}

	bool IsMouseButtonDown(int button)
	{
		return glfwGetMouseButton(window, button) == 1; 
	}

	glm::vec2 GetMousePos()
	{
		double x, y;
		glfwGetCursorPos(this->window, &x, &y);
	
		return glm::vec2(x, y);
	}

private:
	GLFWwindow* window;
	bool * keys;
	bool * mouseButtons;

};
