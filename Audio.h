#pragma once

#include <irrKlang.h>


class Audio
{
public:

	Audio(const char* path, irrklang::ik_f32 volume)
	{
		this->engine = irrklang::createIrrKlangDevice(irrklang::ESOD_AUTO_DETECT);

		if (!engine)
		{
			printf("Couldn't create a irrklang context");
			exit(0);
		}


		this->path = path;
		this->volume = volume;
		this->running = true;


	}

	void Play()
	{

		this->engine->play2D(path, true);
		this->engine->setSoundVolume(volume);


	}

	void SetVolume(irrklang::ik_f32 volume)
	{

		this->volume = volume;
		this->engine->setSoundVolume(this->volume);

	}

	void Stop()
	{
		this->running = false;
		this->engine->stopAllSounds();
	}

	bool IsBeingPlayed()
	{
		return running;
	}

private: 
	irrklang::ISoundEngine* engine;
	const char* path;
	irrklang::ik_f32 volume;
	bool running;
};

