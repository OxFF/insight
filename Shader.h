#pragma once

#include <string>
#include <glad/glad.h>

class Shader 
{
public:
	Shader(const char * vertexShaderPath, const char * fragmentShaderPath)
	{
		const char* vertSrc = getSource(vertexShaderPath);
		const char* fragSrc = getSource(fragmentShaderPath);

		int success;
		char infoLog[512];

		unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, 1, &vertSrc, NULL);
		glCompileShader(vertexShader);

		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
			printf("Error: %s\n", infoLog);
		}

		unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, 1, &fragSrc, NULL);
		glCompileShader(fragmentShader);

		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
			printf("Error: %s\n", infoLog);
		}

		this->shader = glCreateProgram();
		glAttachShader(this->shader, vertexShader);
		glAttachShader(this->shader, fragmentShader);
		glLinkProgram(this->shader);

		glGetProgramiv(this->shader, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(this->shader, 512, NULL, infoLog);
			printf("Error: %s\n", infoLog);
		}

		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		glGenVertexArrays(1, &this->VAO);
		glGenBuffers(1, &this->VBO);

		glBindVertexArray(this->VAO);

		delete[] vertSrc, fragSrc;
	}

	void bind()
	{
		glUseProgram(shader);
	}

	void unBind()
	{
		glUseProgram(0);
	}

	unsigned int getProgram()
	{
		return shader;
	}

	void setFloat(const std::string &name, float value) const
	{
		glUniform1f(glGetUniformLocation(shader, name.c_str()), value);
	}

private:
	const char* getSource(const char* path)
	{

		char* buffer = 0;
		long length;
		FILE * f = fopen(path, "rb"); //was "rb"

		if (f)
		{
			fseek(f, 0, SEEK_END);
			length = ftell(f);
			fseek(f, 0, SEEK_SET);
			buffer = (char*)malloc((length + 1) * sizeof(char));
			if (buffer)
			{
				fread(buffer, sizeof(char), length, f);
			}
			fclose(f);
		}
		buffer[length] = '\0';

		return buffer;
	}

	unsigned int VBO, VAO, shader;
};
