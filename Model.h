
#include "Texture.h"
#include "Shader.h"

#define len(x) (sizeof(x) / sizeof(x[0]))

class Model 
{
public:

	Model(float(&vertices)[512])
	{

		unsigned int VBO, EBO;

		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		// Position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid *)0);
		glEnableVertexAttribArray(0);
		// Color attribute
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid *)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
		// Texture Coordinate attribute
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid *)(6 * sizeof(GLfloat)));
		glEnableVertexAttribArray(2);

		this->NOV = len(vertices) / 8;

		glBindVertexArray(0); // Unbind VAO

	}

	void render()
	{
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, NOV);

		glBindVertexArray(0);

	}


	void remove()
	{
		glDeleteVertexArrays(1, &this->VAO);

		free(&VAO);
	}

	unsigned int getVertexArray()
	{
		return VAO;
	}


private:
	unsigned int VAO;
	int NOV;
};
